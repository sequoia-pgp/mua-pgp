PANDOCOPTS = --standalone --number-sections --toc --toc-depth=2 "-Vdate=$(shell git describe)" --filter pandoc-filter-diagram
HTMLOPTS = -H sq.css

.SUFFIXES: .md .html .pdf

.md.html:
	pandoc  $(PANDOCOPTS) $(HTMLOPTS) --output $@ $<

.md.pdf:
	pandoc $(PANDOCOPTS) --output $@ $<

all: mua-pgp.html mua-pgp.pdf

mua-pgp.html mua-pgp.pdf: mua-pgp.md sq.css Makefile
