# OpenPGP and mail user agents

This is a document on how adding OpenPGP support to mail user agents,
from the Sequoia PGP project.

The rendered version of this guide is available [in
HTML](https://sequoia-pgp.gitlab.io/mua-pgp/) and [in
PDF](https://sequoia-pgp.gitlab.io/mua-pgp/mua-pgp.pdf).
