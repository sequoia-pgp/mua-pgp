---
title: "OpenPGP and mail user agents"
subtitle: "Implementation thoughts"
author: "The Sequoia-PGP project"
documentclass: article
...

[mail user agents]: https://en.wikipedia.org/wiki/Email_client
[Sequoia PGP]: https://sequoia-pgp.org/
[autocrypt]: https://autocrypt.org/
[level 1]: https://autocrypt.org/level1.html
[SDP]: https://datatracker.ietf.org/doc/html/draft-dkg-openpgp-stateless-cli-02

# Introduction

Many [mail user agents][] support OpenPGP in some form. In this document,
the [Sequoia PGP][] project collects thoughts on how to do it well.
The document is aimed at those who implement mail user agents.

# Base level: autocrypt

[Autocrypt][] is described by its website as a:

>  set of guidelines for developers to achieve convenient
>  end-to-end-encryption of e-mails. It specifies how e-mail programs
>  negotiate encryption capabilities using regular e-mails.

The current specification is [level 1][]. It protects against _passive
attackers_, who eavesdrop, but do not try to modify messages,
substitute encryption keys, or otherwise interfere with the
communication. That's a good base level for everyone to have, even if
it's not enough for everyone. Furthermore, autocrypt makes use of
cryptography sufficiently easy that it gets used by default rather
than only if there's a strong need.

With autocrypt:

* When Alice sends a message to Bob, Alice's certificate (public key)
  is automatically attached.
* When Bob responds, his MUA encrypts the response using the
  certificate in Alice's message. This way, Bob never needs to try to
  find the key elsewhere, or to try to form a trust chain to the key
  they find. Bob can implicitly trust Alice's key.
* All email can be encrypted if the sender has the recipient's
  certificate.
* All email can be signed automatically, and all signatures can be
  checked every time, since the sender's certificate is always
  included.

Compared to the situation without autocrypt:

* Alice can attache a certificate, but it's a manual operation.
* Bob can extract the certificate, but it's a manual operation.
* If Alice didn't attach a certificate, Bob needs to find it via key
  servers, WKD, keyoxide, or other means.

In other words, autocrypt takes care of most of the chore of dealing
with certificate distribution and management.

## autocrypt limitations

Autocrypt doesn't protect against active attackers who interfere with
the communication. For example, there's no protection against an
attacker substituting Alice's certificate with one controlled by the
attacker. There's nothing inherent in autocrypt to authenticate an
attached certificate.

The way autocrypt is specified, it may be hard to implement using an
OpenPGP implementation that uses "key rings". A [SOP][] interface
(Stateless OpenPGP Command Line Interface) may work better.

# Recommendations

* First, implement autocrypt.
  - even partial support is useful: e.g., emit and consume autocrypt
    headers
* Make encryption and signature verification a normal, default part of
  the workflow of using email.
  - always check signature of received mail, when sender's certificate
    is available
  - always encrypt sent mail, if recipient's certificate is available
  - make it clear in the user interface which parts of an email are
    covered by a signature
* Add support for additional ways of finding certificates, and for
  building trust in them.
  - key servers
  - [web key directory](https://wiki.gnupg.org/WKD) (WKD)
  - [web of trust](https://sequoia-pgp.gitlab.io/sequoia-wot/)
* Add support for pet names.
  - map contacts in the MUA address book to their OpenPGP certificates
* Add support for sharing certificates between applications.
  - [Sequoia cert-d](https://gitlab.com/sequoia-pgp/pgp-cert-d)
* Add policies for use of cryptography.
  - require recipient certificate to be authenticated in some way
    before using it to encrypt a message
